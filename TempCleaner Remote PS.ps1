function Test-Administrator {
    $currentUser = [Security.Principal.WindowsIdentity]::GetCurrent()
    $principal = New-Object Security.Principal.WindowsPrincipal($currentUser)
    return $principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)
}

if (-not (Test-Administrator)) {
    Write-Output "Restarting script as Administrator..."
    Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs
    exit
}

$currentUsername = [System.Security.Principal.WindowsIdentity]::GetCurrent().Name
Write-Output "Current user: $currentUsername"

$tempFolderPath = [System.IO.Path]::GetTempPath()
Write-Output "Temp folder path: $tempFolderPath"

function Clear-TempFolder {
    param (
        [string]$folderPath
    )

    $items = Get-ChildItem -Path $folderPath -Force -Recurse

    foreach ($item in $items) {
        try {
            Remove-Item -Path $item.FullName -Force -Recurse -ErrorAction Stop
            Write-Output "Deleted: $($item.FullName)"
        } catch {
            Write-Warning "Could not remove item: $($item.FullName). It might be in use by another process."
        }
    }
}

try {
    Clear-TempFolder -folderPath $tempFolderPath
    Write-Output "All deletable files in the Temp folder have been deleted. This window will close automatically."
    Start-Sleep -Seconds 5
    exit
} catch {
    Write-Error "An error occurred while deleting files in the Temp folder: $_"
}
